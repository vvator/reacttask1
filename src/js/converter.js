'use strict';
let MeasurementUnitsConverter = (function() {

    function isConversionObject() {
        if (arguments.length == 0 ||
            arguments.length != 2 ||
            typeof conversion != 'object') {
            return false;
        }
        let obj = arguments[0];
        if (('from' in obj) && ('to' in obj)) {
            return true;
        } else {
            return false;
        }
    }

    function createConverter() {
        if (arguments.length == 0) {
            return;
        }
        let obj = arguments[0];
        if (isConversionObject(obj)) {
            return ConverterFactory.getInstanceFromFactoryMethod(obj);
        }
        return ConverterFactory.getInstanceFromFactoryMethod(obj);
    }

    return {
        createConverter: createConverter
    };
}());

export { MeasurementUnitsConverter };


class ConverterFactory {
    static getInstanceFromFactoryMethod() {
        if (arguments.length == 0) return new Converter();
        let obj = arguments[0];

        if ((obj.from === 'cm') && (obj.to === 'mm')) {
            return new CmToMmConverter();
        }
        if ((obj.from === 'cm') && (obj.to === 'inch')) {
            return new CmToInchConverter();
        }
        if ((obj.from === 'cm') && (obj.to === 'dm')) {
            return new CmToDmConverter();
        }
        if ((obj.from === 'cm') && (obj.to === 'm')) {
            return new CmToMetrConverter();
        }
        if ((obj.from === 'm') && (obj.to === 'km')) {
            return new MetrToKmConverter();
        }

        return new NotImplConverter();
    }
}

class Converter {
    convert() {
        if (arguments.length == 0) {
            return "Not supported conversion";
        }
        if (arguments.length == 1) {
            return arguments[0];
        }

        let contertObj = arguments[0];
        let val = arguments[1];

        for (let i = 0; i < contertObj.length; i++) {
            if (typeof contertObj[i] === 'number') {
                return (contertObj[i] * val);
            }

            if (typeof contertObj[i] === 'string') {
                return new String(contertObj[i] * val);
            }

            if (typeof contertObj[i] === 'object') {
                if (Array.isArray(contertObj[i])) {
                    let arr = [];
                    let tempArr = [];
                    tempArr = contertObj[i];
                    for (let j = 0; j < tempArr.length; j++) {
                        arr.push(tempArr[j] * val);
                    }
                    return arr;
                }
            }
        }

        return '';
    }
};


class NotImplConverter extends Converter {
    convert() {
        return super.convert();
    }
};

class CmToMmConverter extends Converter {
    convert() {
        return super.convert(arguments, 10);
    }
};

class CmToInchConverter extends Converter {
    convert() {
        return super.convert(arguments, 2.48);
    }
};

class CmToDmConverter extends Converter {
    convert() {
        return super.convert(arguments, 1 / 10);
    }
};

class CmToMetrConverter extends Converter {
    convert() {
        return super.convert(arguments, 100);
    }
};

class MetrToKmConverter extends Converter {
    convert() {
        return super.convert(arguments, 1 / 1000);
    }
};