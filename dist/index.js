'use strict';

var _converter = require('./js/converter');

var log = console.log.bind();
var converterModule = _converter.MeasurementUnitsConverter;

function test(objConversion, val) {
    log('Test:');
    log('conversion object:');
    log(objConversion);
    var converter = converterModule.createConverter(objConversion);
    log('result:');
    log(val + ' ' + objConversion.from + ' ===> ' + converter.convert(val) + ' ' + objConversion.to);
    log();
}

log('TESTS:');
log('NUMBER CONVERSION:');
test({ from: 'cm', to: 'mm' }, 100);
test({ from: 'cm', to: 'dm' }, 8);
test({ from: 'cm', to: 'm' }, 15);
test({ from: 'cm', to: 'inch' }, 3);
test({ from: 'm', to: 'km' }, 800);

log('STRING CONVERSION:');
test({ from: 'cm', to: 'mm' }, '200');
test({ from: 'cm', to: 'dm' }, '80');
test({ from: 'cm', to: 'm' }, '150');
test({ from: 'cm', to: 'inch' }, '30');
test({ from: 'm', to: 'km' }, '1800');

log('ARRAY CONVERSION:');
test({ from: 'cm', to: 'mm' }, [100, 200, 300, 400, 500]);
test({ from: 'cm', to: 'inch' }, [10, 20], [30, 40, 50, 88]);
test({ from: 'm', to: 'km' }, [33, 300], [40, 48, 5000], [878]);

log('UNSUPPORTED CONVERSION:');
test({ from: 'FAKE', to: 'km' }, 800);
test({ from: 'cm', to: 'FAKE' }, 8);
test({ from: 'km', to: 'mm' }, 2);