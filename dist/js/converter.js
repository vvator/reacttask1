'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MeasurementUnitsConverter = function () {

    function isConversionObject() {
        if (arguments.length == 0 || arguments.length != 2 || (typeof conversion === 'undefined' ? 'undefined' : _typeof(conversion)) != 'object') {
            return false;
        }
        var obj = arguments[0];
        if ('from' in obj && 'to' in obj) {
            return true;
        } else {
            return false;
        }
    }

    function createConverter() {
        if (arguments.length == 0) {
            return;
        }
        var obj = arguments[0];
        if (isConversionObject(obj)) {
            return ConverterFactory.getInstanceFromFactoryMethod(obj);
        }
        return ConverterFactory.getInstanceFromFactoryMethod(obj);
    }

    return {
        createConverter: createConverter
    };
}();

exports.MeasurementUnitsConverter = MeasurementUnitsConverter;

var ConverterFactory = function () {
    function ConverterFactory() {
        _classCallCheck(this, ConverterFactory);
    }

    _createClass(ConverterFactory, null, [{
        key: 'getInstanceFromFactoryMethod',
        value: function getInstanceFromFactoryMethod() {
            if (arguments.length == 0) return new Converter();
            var obj = arguments[0];

            if (obj.from === 'cm' && obj.to === 'mm') {
                return new CmToMmConverter();
            }
            if (obj.from === 'cm' && obj.to === 'inch') {
                return new CmToInchConverter();
            }
            if (obj.from === 'cm' && obj.to === 'dm') {
                return new CmToDmConverter();
            }
            if (obj.from === 'cm' && obj.to === 'm') {
                return new CmToMetrConverter();
            }
            if (obj.from === 'm' && obj.to === 'km') {
                return new MetrToKmConverter();
            }

            return new NotImplConverter();
        }
    }]);

    return ConverterFactory;
}();

var Converter = function () {
    function Converter() {
        _classCallCheck(this, Converter);
    }

    _createClass(Converter, [{
        key: 'convert',
        value: function convert() {
            if (arguments.length == 0) {
                return "Not supported conversion";
            }
            if (arguments.length == 1) {
                return arguments[0];
            }

            var contertObj = arguments[0];
            var val = arguments[1];

            for (var i = 0; i < contertObj.length; i++) {
                if (typeof contertObj[i] === 'number') {
                    return contertObj[i] * val;
                }

                if (typeof contertObj[i] === 'string') {
                    return new String(contertObj[i] * val);
                }

                if (_typeof(contertObj[i]) === 'object') {
                    if (Array.isArray(contertObj[i])) {
                        var arr = [];
                        var tempArr = [];
                        tempArr = contertObj[i];
                        for (var j = 0; j < tempArr.length; j++) {
                            arr.push(tempArr[j] * val);
                        }
                        return arr;
                    }
                }
            }

            return '';
        }
    }]);

    return Converter;
}();

;

var NotImplConverter = function (_Converter) {
    _inherits(NotImplConverter, _Converter);

    function NotImplConverter() {
        _classCallCheck(this, NotImplConverter);

        return _possibleConstructorReturn(this, (NotImplConverter.__proto__ || Object.getPrototypeOf(NotImplConverter)).apply(this, arguments));
    }

    _createClass(NotImplConverter, [{
        key: 'convert',
        value: function convert() {
            return _get(NotImplConverter.prototype.__proto__ || Object.getPrototypeOf(NotImplConverter.prototype), 'convert', this).call(this);
        }
    }]);

    return NotImplConverter;
}(Converter);

;

var CmToMmConverter = function (_Converter2) {
    _inherits(CmToMmConverter, _Converter2);

    function CmToMmConverter() {
        _classCallCheck(this, CmToMmConverter);

        return _possibleConstructorReturn(this, (CmToMmConverter.__proto__ || Object.getPrototypeOf(CmToMmConverter)).apply(this, arguments));
    }

    _createClass(CmToMmConverter, [{
        key: 'convert',
        value: function convert() {
            return _get(CmToMmConverter.prototype.__proto__ || Object.getPrototypeOf(CmToMmConverter.prototype), 'convert', this).call(this, arguments, 10);
        }
    }]);

    return CmToMmConverter;
}(Converter);

;

var CmToInchConverter = function (_Converter3) {
    _inherits(CmToInchConverter, _Converter3);

    function CmToInchConverter() {
        _classCallCheck(this, CmToInchConverter);

        return _possibleConstructorReturn(this, (CmToInchConverter.__proto__ || Object.getPrototypeOf(CmToInchConverter)).apply(this, arguments));
    }

    _createClass(CmToInchConverter, [{
        key: 'convert',
        value: function convert() {
            return _get(CmToInchConverter.prototype.__proto__ || Object.getPrototypeOf(CmToInchConverter.prototype), 'convert', this).call(this, arguments, 2.48);
        }
    }]);

    return CmToInchConverter;
}(Converter);

;

var CmToDmConverter = function (_Converter4) {
    _inherits(CmToDmConverter, _Converter4);

    function CmToDmConverter() {
        _classCallCheck(this, CmToDmConverter);

        return _possibleConstructorReturn(this, (CmToDmConverter.__proto__ || Object.getPrototypeOf(CmToDmConverter)).apply(this, arguments));
    }

    _createClass(CmToDmConverter, [{
        key: 'convert',
        value: function convert() {
            return _get(CmToDmConverter.prototype.__proto__ || Object.getPrototypeOf(CmToDmConverter.prototype), 'convert', this).call(this, arguments, 1 / 10);
        }
    }]);

    return CmToDmConverter;
}(Converter);

;

var CmToMetrConverter = function (_Converter5) {
    _inherits(CmToMetrConverter, _Converter5);

    function CmToMetrConverter() {
        _classCallCheck(this, CmToMetrConverter);

        return _possibleConstructorReturn(this, (CmToMetrConverter.__proto__ || Object.getPrototypeOf(CmToMetrConverter)).apply(this, arguments));
    }

    _createClass(CmToMetrConverter, [{
        key: 'convert',
        value: function convert() {
            return _get(CmToMetrConverter.prototype.__proto__ || Object.getPrototypeOf(CmToMetrConverter.prototype), 'convert', this).call(this, arguments, 100);
        }
    }]);

    return CmToMetrConverter;
}(Converter);

;

var MetrToKmConverter = function (_Converter6) {
    _inherits(MetrToKmConverter, _Converter6);

    function MetrToKmConverter() {
        _classCallCheck(this, MetrToKmConverter);

        return _possibleConstructorReturn(this, (MetrToKmConverter.__proto__ || Object.getPrototypeOf(MetrToKmConverter)).apply(this, arguments));
    }

    _createClass(MetrToKmConverter, [{
        key: 'convert',
        value: function convert() {
            return _get(MetrToKmConverter.prototype.__proto__ || Object.getPrototypeOf(MetrToKmConverter.prototype), 'convert', this).call(this, arguments, 1 / 1000);
        }
    }]);

    return MetrToKmConverter;
}(Converter);

;